<?php
session_start();
$name = $_SESSION["name"];
$gender = $_SESSION["gender"];
$khoa = $_SESSION["khoa"];
$address = $_SESSION["address"];
$date = $_SESSION["date"];
$file = $_SESSION["file"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="register.css">
    <title>Document</title>
</head>
<body>
    <form method="post" action="">
        <div class="elements">
            <label class="label-input">Họ và tên </label>
            <p class=""><?php echo $name ?></p>
        </div>
        <div class="elements">
            <label class="label-input">Giới tính</label>
            <p class=""> <?php echo $gender ?></p>
        </div>
        <div class="elements">
            <label class="label-input">Phân Khoa </label>
            <p class=""> <?php echo $khoa ?></p>
        </div>
        <div class="elements">
          <label class="label-input date" for="date">Ngày sinh </label>
          <p class=""> <?php echo $date ?></p>
        </div>
        <div class="elements">
            <label class="label-input">Địa chỉ</label>
            <p class=""> <?php echo $address ?></p>
        </div>
        <div class="img">
            <div>
            <label class="label-input">Hình ảnh</label>
            </div>
            <div>
            <img src="<?php echo $file['file']['name'] ;?>" alt="" width="100" height="65">
            </div>
        </div>

        <button class="reigster">Xác nhận</button>
    </form>
</body>
</html>